*** Setting ***

Library    AppiumLibrary
Library    BuiltIn
Library    FakerLibrary
Library    String

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Keywords ***
Register D2D
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # register d2d
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
  #masuk ke halaman register
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="btnregister, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="btnregister, "]
  # create data dummy
  # create first name
  ${firstname}=    First Name Female
  ${firstname}=   Set Variable    TEST ${firstname}
  Log    ${firstname}
  # create last name
  ${lastname}=  Last Name Female
  Log    ${lastname}
  # create email yang belum ada agar muncul tombol daftar
  ${count}=  Numerify   text=####
  ${emailnew}=  Set Variable    ${lastname}test${count}@yopmail.com
  Convert To Lowercase    ${emailnew}
  Log    ${emailnew}
  Set Global Variable    ${emailnew}
  #input data Register
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Input Your Data')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'REGISTER')]    ${timeout}
  # input fullname
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_fullname, "]   ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_fullname, "]    ${firstname} ${lastname}
  Hide Keyboard
  # input email
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]    ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_email, "]    ${emailnew}
  Hide Keyboard
  # input password
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_password, "]    ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_password, "]    ${pass_regis}
  Hide Keyboard
  # input confirm password
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_confirm_password, "]   ${timeout}
  # Tap    //android.widget.EditText[@content-desc="input_confirm_password, "]
  Input Text    //android.widget.EditText[@content-desc="input_confirm_password, "]    ${pass_regis}
  Hide Keyboard
  #klik tombol register -> harus 2x
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat tombol Register
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_register, "]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
    #klik tombol register
    #klik tombol register -> harus 2x
    : FOR    ${loopCount}    IN RANGE    0    2
    \    ${el}    Run Keyword And Return Status    Page Should Not Contain Element    //android.view.ViewGroup[@content-desc="button_register, "]
    \    Run Keyword If    ${el}    Exit For Loop
    \    Click Element    //android.view.ViewGroup[@content-desc="button_register, "]
    \    Sleep    2s
    \    ${loopCount}    Set Variable    ${loopCount}+1
    Sleep    1s
    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Email Verification')]   ${timeout}
    #kembali ke halaman login
    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]     ${timeout}
    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
    Close Application

Aktivasi Akun
    #aktivasi akun dengan membuka email
    #ambil kode dulu di email
    #buka chrome
    Open Application    ${REMOTE_URL}    platformName=${PLATFORM_NAME}    platformVersion=${PLATFORM_VERSION_REAL}    deviceName=${DEVICE_NAME_REAL}    appPackage=${APP_PACKAGE_CHROME}
    ...    appActivity=${APP_ACTIVITY_CHROME}   automationName=${automationName}      noReset=true    waitForQuiescence=false
    #newtab
    Wait Until Page Contains Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/tab_switcher_button"]    ${timeout}
    Click Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/tab_switcher_button"]
    Wait Until Page Contains Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/new_tab_button"]    ${timeout}
    Click Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/new_tab_button"]
    #cek halaman awal google
    #masuk yopmail
    Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]   5s
    Click Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]
    Input Text    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/url_bar')]    yopmail.com
    Press Keycode    66
    Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'login')]   ${timeout}
    Clear Text    //android.widget.EditText[contains(@resource-id,'login')]
    Input Text    //android.widget.EditText[contains(@resource-id,'login')]    ${emailnew}
    # Hide Keyboard
    Sleep    1s
    Tap    //android.widget.EditText[contains(@resource-id,'login')]
    Press Keycode    66
    Wait Until Page Contains Element    //android.view.View[contains(@text,'today')]    ${timeout}
    Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'m1')]    ${timeout}
    #buka email
    Click Element    //android.view.View[contains(@resource-id,'m1')]
    Wait Until Page Contains Element    //android.view.View[contains(@text,'D2D Account Activation')]   ${timeout}
    Wait Until Page Contains Element    //android.view.View[@text="ACTIVATE"][@clickable="true"]   ${timeout}
    Sleep    2s
    #klik aktivasi
    Click Element    //android.view.View[@text="ACTIVATE"][@clickable="true"]
    ${deeplink}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Buka dengan')]   10s
    Run Keyword If    ${deeplink}    Click Element    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.ScrollView/android.widget.LinearLayout/com.android.internal.widget.ViewPager/android.widget.LinearLayout/android.widget.GridView/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.ImageView
    Wait Until Page Contains Element    //android.widget.ScrollView[contains(@resource-id,'com.android.chrome:id/modal_dialog_scroll_view')]    ${timeout}
    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.android.chrome:id/message')][@text='Selamat, akun anda telah terverifikasi.']    ${timeout}
    #back & hapus semua email yopmail
    Wait Until Page Contains Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/tab_switcher_button')]    ${timeout}
    Click Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/tab_switcher_button')]
    Wait Until Page Contains Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/new_tab_button')]    ${timeout}
    Click Element    //android.widget.ImageButton[contains(@resource-id,'com.android.chrome:id/new_tab_button')]
    # #cek halaman awal google
    # #masuk yopmail
    # Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]   5s
    # Click Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]
    # Input Text    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/url_bar')]    yopmail.com
    # Press Keycode    66
    # Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'login')]   ${timeout}
    # Clear Text    //android.widget.EditText[contains(@resource-id,'login')]
    # Input Text    //android.widget.EditText[contains(@resource-id,'login')]    ${email_regis}
    # Hide Keyboard
    # Sleep    1s
    # Press Keycode    66
    # Wait Until Page Contains Element    //android.view.View[contains(@text,'today')]    ${timeout}
    # Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'m1')]    ${timeout}
    # Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'rethome')]   ${timeout}
    # Click Element    //android.view.View[contains(@resource-id,'rethome')]
    # Sleep    2s
    # Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'e0')]    ${timeout}
    # Click Element    //android.view.View[contains(@resource-id,'e0')]
    # Sleep    2s
    # Click Element    //android.view.View[contains(@text,'javascript:suppr_sel();')]
    # Sleep    1s
    # Press Keycode    187  #recent apps
    # Sleep    1s
    # Press Keycode    187  #recent apps
    # Sleep    1s
    # #kembali ke D2D - halaman login
    # Wait Until Page Contains Element    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView
    # Click Element    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView
    # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
    # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
    # Sleep    ${timeout}
