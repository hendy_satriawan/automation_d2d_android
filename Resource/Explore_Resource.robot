*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    DateTime

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
${cari_event}   KONKER PDPI 2019
${cari_spesialis}     Spesialis Bedah Saraf
${request_judul}   [TEST DUMMY] - Hiraukan Saja Request Ini
${pilihobat}     White petrolatum
${cariobat}   Albumin
${cari_event_berbayar}    International Symposium on Emergency Medicine (ISOE) II 2020
# ${judul_event_ecertificate}   10th Surabaya Cardiology Update
${judul_event_ecertificate}   PITNAS Perhimpunan Osteoporosis Indonesia 2019 (PEROSI)
${nama_gelar}   dr. QA Tester
*** Keywords ***
Explore Event List
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]
  #pilih Event
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonEvent')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonEvent')]

Event detail
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Event')]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
  ${tanggal}    Get Current Date    result_format=%b
  ${tanggal2}    Get Current Date    result_format=%m
  Log    ${tanggal}
  Log    ${tanggal2}
  #masuk ke detail event
  ${cekeventbulanini}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${tanggal}')]
  Run Keyword If    ${cekeventbulanini}    Click Element    //android.widget.TextView[contains(@text,'${tanggal}')]
  Run Keyword Unless    ${cekeventbulanini}    Click Element    //android.view.ViewGroup[@content-desc="buttonOnClick, "]
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'button_back')]    ${timeout}
  # # cari tombol RSVP
  # ${lebarx}    Get Window Width
  # ${tinggiy}   Get Window Height
  # ${lebarx}   Convert To Integer    ${lebarx}
  # ${tinggiy}  Convert To Integer    ${tinggiy}
  # ${lebars}   Evaluate    ${lebarx}/2
  # ${tinggis}    Evaluate    ${tinggiy} - 200
  # ${x1-rsvp}   Convert To String    ${lebars}
  # ${x2-rsvp}   Convert To String    ${lebars}
  # ${y1-rsvp}   Convert To String    ${tinggis}
  # ${y2-rsvp}   Evaluate    ${tinggis} - 500
  # #Scroll detail event sampai dapat judul yang dimaksud (tipe journal)
  # : FOR    ${loopCount}    IN RANGE    0    20
  # \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //android.view.ViewGroup[contains(@content-desc,'buttonRSVP')]
  # \    Run Keyword If    ${el}    Exit For Loop
  # \    Swipe    ${x1-rsvp}    ${y1-rsvp}    ${x2-rsvp}    ${y2-rsvp}
  # \    ${loopCount}    Set Variable    ${loopCount}+1
  # Sleep    1s
  # Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonRSVP')]
  # #masuk konfirmasi hadir
  # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Contact the following Contact Person for a formal reservation')]    ${timeout}
  # ${coming_no}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonAttend')]
  # ${coming_yes}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonDoYouWantToAttend')]
  # Run Keyword If    ${coming_yes}    Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonDoYouWantToAttend')]
  # Run Keyword If    ${coming_no}    Run Keywords    Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonAttend')]
  # ...   AND   Log    Event sudah direservasi

Kembali ke detail event
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'button_back')]    ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'button_back')]

Bookmark Event Detail
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Bookmark')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Bookmark')]
  # confirm add to calendar
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'android:id/message')]    ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'android:id/button2')][@text="LATER"]
  Click Element    //android.widget.Button[contains(@resource-id,'android:id/button2')][@text="LATER"]
  #bookmark succesfull snackbar
  # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'bookmark succesfull')]    ${timeout}
  Sleep    2s

Unbookmark Event Detail
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Bookmark')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Bookmark')]
  #unbookmark succesfull snackbar
  # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Unbookmark succesfull')]    ${timeout}
  Sleep    2s

Share Event
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_share, "]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_share, "]
  #muncul pilihan share
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'android:id/title_default')]    ${timeout}
  Press Keycode    4    #back

Add Event To Calendar
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Calendar')]    ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Calendar')]
  Permission_Calendar
  #muncul pilihan share
  Wait Until Page Contains Element    //android.widget.Button[contains(@resource-id,'com.samsung.android.calendar:id/action_done')]   ${timeout}
  Click Element    //android.widget.Button[contains(@resource-id,'com.samsung.android.calendar:id/action_done')]
  Wait Until Page Does Not Contain Element    //android.widget.Button[contains(@resource-id,'com.samsung.android.calendar:id/action_done')]   ${timeout}

Kembali ke List Event
  Sleep    2s
  Press Keycode    4    #back
  Wait Until Page Does Not Contain Element    //android.widget.TextView[contains(@text,'Calendar')]    ${timeout}
  # #kembali ke event List
  # Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'button_back')]   ${timeout}
  # Click Element    //android.view.ViewGroup[contains(@content-desc,'button_back')]
  # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Event')]    ${timeout}
  # Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'button_bookmark')]    ${timeout}


Search Event
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'button_search')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'button_search')]
  #masuk ke halaman search
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'We are really sad we could not find anything')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search, "]   ${timeout}
  # lakukan search
  Input Text    //android.widget.EditText[@content-desc="input_search, "]    ${cari_event}
  Hide Keyboard
  # masuk ke hasil search
  ${carievent}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonOnClick, "]
  Run Keyword If    ${carievent}    Run Keywords    Click Element    //android.view.ViewGroup[@content-desc="buttonOnClick, "]
  ...   AND     Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${cari_event}')]    ${timeout}

Kembali dari pencarian event ke list event
  ${backlist}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]
  Run Keyword If    ${backlist}    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  # ke list event
  Press Keycode    4    #back
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Event')]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}

Pilih tab bookmark
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'BOOKMARK')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'BOOKMARK')]
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonBookmark')]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonShare')]   ${timeout}

Filter Event
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_filter, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_filter, "]
  # cek filter yang muncul
  ${bulan_filter}   Get Current Date
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'January')]    ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'January')]
  # keluar dari opsi filter
  Press Keycode    4    #back



Explore Learning List
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]
  #pilih Learning
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonLearning')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonLearning')]

Explore Detail List
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Learning')]     ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_learning_search, "]    ${timeout}
  # pilig spesialis anak
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Spesialis Anak')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Spesialis Anak')]
  # cek halaman spesialis anak
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]   ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_search, "]   ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonItemOnClick, "]    ${timeout}
  # masuk ke detail konten
  Click Element    //android.view.ViewGroup[@content-desc="buttonItemOnClick, "]
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_more, "]   ${timeout}
  # Bookmark
  Click Element    //android.view.ViewGroup[@content-desc="button_more, "]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Bookmark')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Bookmark')]
  # kembali ke list konten
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Page Should Not Contain Element    //android.view.ViewGroup[@content-desc="button_back, "]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  \    Sleep    2s
  \    ${loopCount}    Set Variable    ${loopCount}+1

Explore Tab Bookmark
  Wait Until Page Contains Element    //android.widget.TextView[@text="BOOKMARK"]   ${timeout}
  Click Element    //android.widget.TextView[@text="BOOKMARK"]
  # unbookmark
  ${judul_cari}   Get Text    //android.widget.TextView[@content-desc="titleJournal, "]
  Log    ${judul_cari}
  Set Global Variable    ${judul_cari}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonItemOnClick, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="buttonItemOnClick, "]
  Sleep    1s
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_more, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_more, "]
  Sleep    1s
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Unbookmark')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Unbookmark')]
  # keluar dari detail konten
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Page Should Not Contain Element    //android.view.ViewGroup[@content-desc="button_back, "]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  \    Sleep    2s
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # refresh
  Wait Until Page Contains Element    //android.widget.TextView[@text="LATEST"]
  Click Element    //android.widget.TextView[@text="LATEST"]
  Sleep    1s
  Wait Until Page Contains Element    //android.widget.TextView[@text="BOOKMARK"]
  Click Element    //android.widget.TextView[@text="BOOKMARK"]

Search Konten Learning
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_search, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_search, "]
  # input search
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search, "]   ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_search, "]    ${judul_cari}
  Hide Keyboard
  # buka semua tab
  Wait Until Page Contains Element    //android.widget.TextView[@text="GUIDELINE"]    ${timeout}
  Click Element    //android.widget.TextView[@text="GUIDELINE"]
  Wait Until Page Contains Element    //android.widget.TextView[@text="JOURNAL"]    ${timeout}
  Click Element    //android.widget.TextView[@text="JOURNAL"]
  # keluar dari search konten
  Sleep    2s
  Press Keycode    4    #back
  # kembali ke list spesialis
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]

Cari Spesialis
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_learning_search, "]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_learning_search, "]
  # input cari spesialis
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search, "]    ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_search, "]    ${cari_spesialis}
  Hide Keyboard
  # dapat yang dicari lalu klik
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonSpecialist, "]    ${timeout}
  Page Should Not Contain Element    //android.view.ViewGroup[@content-desc="buttonSpecialist, "][2]
  Click Element    //android.view.ViewGroup[@content-desc="buttonSpecialist, "]
  ## Wait Until Page Contains Element    //android.widget.TextView[@text="Spesialis Bedah Saraf"]    ${timeout}
  ## Click Element    //android.widget.TextView[@text="Spesialis Bedah Saraf"]
  # kembali ke list spesialis
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]   ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_search, "]     ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search, "]    ${timeout}
  Press Keycode    4    #back
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Learning')]     ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_learning_search, "]    ${timeout}

Explore Request Journal
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]
  #pilih Request Journal
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonRequestJournal, "]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="buttonRequestJournal, "]
  # masuk ke halaman Request Journal
  Wait Until Page Contains Element    //android.widget.TextView[@text="Request Journal"]    ${timeout}
  # input city
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_city, "]
  Click Element    //android.widget.EditText[@content-desc="input_city, "]
  # input search city
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search_city, "]    ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_search_city, "]    Jakarta
  Hide Keyboard
  # pilih jakarta selatan
  Wait Until Page Contains Element    //android.widget.TextView[@text="KOTA JAKARTA SELATAN"]   ${timeout}
  Click Element    //android.widget.TextView[@text="KOTA JAKARTA SELATAN"]
  # swipe sampai dapat judul jurnal
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat tombol Register
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_submit, "]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   # input judul journal
   Wait Until Page Contains Element    //android.widget.EditText[@content-desc="title_journal, "]   ${timeout}
   Input Text    //android.widget.EditText[@content-desc="title_journal, "]    ${request_judul}
   Hide Keyboard
   # klik submit
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_submit, "]    ${timeout}
   Click Element    //android.view.ViewGroup[@content-desc="button_submit, "]
   # berhasil Request
   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]    ${timeout}

 Explore Event Dengan Materi
   # swipe sampai dapat judul jurnal
   ${lebarx}    Get Window Width
   ${tinggiy}   Get Window Height
   ${lebarx}   Convert To Integer    ${lebarx}
   ${tinggiy}  Convert To Integer    ${tinggiy}
   ${lebars}   Evaluate    ${lebarx}/2
   ${tinggis}    Evaluate    ${tinggiy} - 200
   ${x1-regis}   Convert To String    ${lebars}
   ${x2-regis}   Convert To String    ${lebars}
   ${y1-regis}   Convert To String    ${tinggis}
   ${y2-regis}   Evaluate    ${tinggis} - 500
   Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]   ${timeout}
   Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]
   # pilih event
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonEvent, "]     ${timeout}
   Click Element    //android.view.ViewGroup[@content-desc="buttonEvent, "]
   # masuk halaman event
   Wait Until Page Contains Element    //android.widget.TextView[@text="Event"]      ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_search, "]     ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_filter, "]     ${timeout}
   # masuk ke tab join
   Wait Until Page Contains Element    //android.widget.TextView[@text="JOINED"]     ${timeout}
   Click Element    //android.widget.TextView[@text="JOINED"]
   Sleep    2s
   # buka event di tab join untuk buka materi
    : FOR    ${loopCount}    IN RANGE    0    2
    \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="${cari_event_berbayar}"]
    \    Run Keyword If    ${el}    Exit For Loop
    \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
    \    ${loopCount}    Set Variable    ${loopCount}+1
   Wait Until Page Contains Element    //android.widget.TextView[@content-desc="iconCheckmark, "]     ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonOnClick, "][@displayed="true"]   ${timeout}
   # Click Element    //android.view.ViewGroup[@content-desc="buttonOnClick, "][@displayed="true"]
   Click Element    //android.widget.TextView[@text="${cari_event_berbayar}"]
   # cari event
   # # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_search"]   ${timeout}
   # # Click Element    //XCUIElementTypeOther[@name="button_search"]
   # # # masuk ke halaman search
   # # Wait Until Page Contains Element    //XCUIElementTypeOther[contains(@name,'input_search')]    ${timeout}
   # # Wait Until Page Contains Element    //XCUIElementTypeStaticText[@name="We are really sad we could not find anything"]     ${timeout}
   # # Wait Until Page Contains Element    //XCUIElementTypeOther[@name="button_close"]    ${timeout}
   # # # lakukan search
   # # Click Element    //XCUIElementTypeOther[@name=" input_search "]
   # # Input Text    //XCUIElementTypeOther[@name=" input_search "]    ${cari_event_berbayar}
   # # Wait Until Page Contains Element    //XCUIElementTypeButton[@name="Return"]     ${timeout}
   # # Click Element    //XCUIElementTypeButton[@name="Return"]
   # # # masuk ke event yang dicari
   # # Wait Until Page Contains Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[4]    ${timeout}
   # # Click Element    xpath=(//XCUIElementTypeOther[@name="buttonOnClick"])[4]
   # cek halaman event dengan materi
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Calendar')]   ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_open_location, "]     ${timeout}
   # Wait Until Page Does Not Contain Element    //XCUIElementTypeOther[@name=" Download Materi Event Material File"]   ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Materi')]    ${timeout}
   # buka materi
   Click Element    //android.widget.TextView[contains(@text,'Materi')]
   # cek halaman materi
   Sleep    2s
   Wait Until Page Contains Element    //android.widget.TextView[@text="Event Material"]   ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[@text="Rundown Acara"]    ${timeout}

Kembali ke List Event Joined
   # balik ke detil event
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]   ${timeout}
   Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
   Wait Until Page Contains Element    //android.widget.TextView[@text="${cari_event_berbayar}"]
   # kembali ke list event dari detil
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]    ${timeout}
   Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
   # cek list event
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_search, "]    ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[@text="Event"]   ${timeout}

Explore Ecertificate
  # swipe sampai dapat judul jurnal
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat tombol Register
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="${judul_event_ecertificate}"]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Click Element    //android.widget.TextView[@text="${judul_event_ecertificate}"]
   # masuk ke detil event
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_share, "]   ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[@text="${judul_event_ecertificate}"]   ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_e_certificate, "]   ${timeout}
   # masuk ke Ecertificate
   Click Element    //android.view.ViewGroup[@content-desc="button_e_certificate, "]
   # cek popup input nama
   ${input_nama}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="Mohon koreksi dan lengkapi nama Anda untuk mendapatkan data sertifikat yang sesuai"]
   Run Keyword If    ${input_nama}    Run Keywords    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_submit, "]
   ...    AND   Input Text    //android.widget.EditText[@content-desc="input_nama_gelar, "]    ${nama_gelar}
   ...    AND   Hide Keyboard
   ...    AND   Click Element    //android.view.ViewGroup[@content-desc="button_submit, "]
   ...    AND   Wait Until Page Contains Element    //android.widget.TextView[@text="Are you sure to use this name to your certificated?"]
   ...    AND   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_yes, "]
   ...    AND   Wait Until Page Contains Element    //android.widget.TextView[@text="${nama_gelar}"]
   ...    AND   Click Element    //android.view.ViewGroup[@content-desc="button_yes, "]
   ...    AND   Wait Until Page Contains Element    //android.widget.TextView[@text="Terima kasih, karena sudah melengkapi nama beserta gelar untuk keperluan sertifikat"]    20s
   ...    AND   Click Element    //android.widget.TextView[@text="x"]
   # cek halaman sertifikat
   Wait Until Page Contains Element    //android.widget.TextView[@text="Certificate"]   ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_download, "]    ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[@text="Sent Email"]    ${timeout}
   # download sertifikat
   Click Element    //android.view.ViewGroup[@content-desc="button_download, "]
   Permission_Storage
   # kirim email
   Wait Until Page Contains Element    //android.widget.TextView[@text="Sent Email"]
   Click Element    //android.widget.TextView[@text="Sent Email"]

Kembali ke List Event Dari Ecertificate
  #swipe sampai dapat tombol Register
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_share, "]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
   \    ${loopCount}    Set Variable    ${loopCount}+1
  # kembali dari detil
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_share, "]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  # cek halaman list event
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_search, "]   ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_filter, "]   ${timeout}

Explore Obat A to Z
  # pilih explore & pilih menu obat a to z
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonExplore')]
  # pilih A to Z
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonAtoZ, "]     ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="buttonAtoZ, "]
  # masuk halaman A to Z
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Obat A to Z')]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_input_cari_obat, "]    ${timeout}
  # Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Abacavir')]     ${timeout}
  # swipe alphabet
  #ambil kordinat element
  ${slidealpabet}    Get Element Location    //android.widget.TextView[@text="E"]
  ${slidealpabet}    Convert To String    ${slidealpabet}
  ${remove}   Remove String    ${slidealpabet}    {  '   y   x    :   }
  Log    ${remove}
  ${subsx}   Fetch From Right    ${remove}    ,
  ${subsx}   Fetch From Left    ${subsx}    .0
  ${subsy}   Fetch From Left    ${remove}    ,
  ${subsy}  Fetch From Left    ${subsy}    .0
  ${subsx2}  Fetch From Right    ${remove}    ,
  ${subsx2}   Fetch From Left    ${subsx2}    .0
  Log    ${subsx}
  Log    ${subsy}
  # #ubah value untuk digunakan untuk swipe
  ${subsx2}    Convert To Integer    ${subsx2}
  ${subsx2}   Evaluate    ${subsx2} - 500
  #swipe kesamping untuk dapat tips yang diinginkan
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="Z"][@displayed="true"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${subsx}    ${subsy}    ${subsx2}    ${subsy}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s

Buka Detil Obat A to Z
  # buka obat dengan huruf W
  Wait Until Page Contains Element    //android.widget.TextView[@text="W"]    ${timeout}
  Click Element    //android.widget.TextView[@text="W"]
  # masuk ke detial obat
  # scroll kebawah cari obat
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat obat yang dicari
   : FOR    ${loopCount}    IN RANGE    0    5
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="${pilihobat}"][@displayed="true"]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s
   Click Element    //android.widget.TextView[@text="${pilihobat}"][@displayed="true"]
   # cek halaman detil obat
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]    ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[@text="${pilihobat}"][@displayed="true"]    ${timeout}
   # see more & less
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]    ${timeout}
   Click Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]
   Sleep    1s
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]    ${timeout}
   Click Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]
   #swipe sampai dapat text related
  : FOR    ${loopCount}    IN RANGE    0    5
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="Related"][@displayed="true"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  # buka related
  Wait Until Page Contains Element    //android.widget.TextView[@text="Heparin"]    ${timeout}
  Click Element    //android.widget.TextView[@text="Heparin"]
  # cek halaman related yang di klik
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[@text="Heparin"]    ${timeout}
  Sleep    2s
  # balik ke detail obat sebelumnya
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  Wait Until Page Contains Element    //android.widget.TextView[@text="${pilihobat}"][@displayed="true"]    ${timeout}
  # kembali ke halaman obat A to Z
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]     ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  # cek halaman obat A to Z
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Obat A to Z')]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_input_cari_obat, "]    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[@text="${pilihobat}"][@displayed="true"]    ${timeout}

Cari Obat A to Z
  # scroll kebawah cari obat
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  # masuk ke halaman cari obat
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_input_cari_obat, "]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_input_cari_obat, "]
  # input obat yang dicari
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search, "]   ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_close, "]    ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_search, "]    ${cariobat}
  Hide Keyboard
  # pilih obat yang dicari
  Wait Until Page Contains Element    //android.widget.TextView[@text="${cariobat}"]    ${timeout}
  Click Element    //android.widget.TextView[@text="${cariobat}"]
  # cek halaman yang dicari
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[@text="${cariobat}"][@displayed="true"]    ${timeout}
  # see more & less
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]
  Sleep    1s
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="show_more_less, "][1]
  #swipe sampai dapat text related
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="Related"][@displayed="true"]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s
   # kembali ke halaman A to Z
   : FOR    ${loopCount}    IN RANGE    0    2
   \    ${ul}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Obat A to Z')]
   \    Run Keyword If    ${ul}    Exit For Loop
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]
   \    Run Keyword If    ${el}    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
   \    Run Keyword Unless    ${el}   Click Element    //android.view.ViewGroup[@content-desc="button_close, "]
   \    Sleep    1s
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s
