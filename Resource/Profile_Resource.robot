*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    DateTime

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
${nama_profile_stag}   User Test
${nama_profile}   Fetuna CH
*** Keywords ***
Explore Profile
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonProfile, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="buttonProfile, "]
  # masuk halaman profile
  Wait Until Page Contains Element    //android.widget.TextView[@text="${nama_profile_stag}"]    ${timeout}

Tab Bookmark
  # masuk ke tab bookmark
  Wait Until Page Contains Element    //android.widget.TextView[@text="BOOKMARK"]   ${timeout}
  Click Element    //android.widget.TextView[@text="BOOKMARK"]
  # masuk ke halaman Bookmark
  Wait Until Page Contains Element    //android.widget.TextView[@text="Bookmark"]   ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]     ${timeout}
  Sleep    4s
  # keluar dari halaman bookmark
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]

Tab Download
  Wait Until Page Contains Element    //android.widget.TextView[@text="DOWNLOAD"]     ${timeout}
  Click Element    //android.widget.TextView[@text="DOWNLOAD"]
  # masuk ke halaman download
  Wait Until Page Contains Element    //android.widget.TextView[@text="Download"]   ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]   ${timeout}
  Sleep    4s
  # keluar dari halaman download
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]

Save Profile
  # cari tombol save dibawah
  # scroll kebawah 3x
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat tombol Save
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //android.view.ViewGroup[@content-desc="button_save, "]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Click Element    //android.view.ViewGroup[@content-desc="button_save, "]

Tab Notifikasi
  # scroll kebawah 3x
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat tombol Save
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //android.view.ViewGroup[@content-desc="tab_NOTIFICATION, "]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Swipe    ${x2-regis}    ${y2-regis}    ${x1-regis}    ${y1-regis}
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="tab_NOTIFICATION, "]   ${timeout}
  ${tombol}   Get Element Location    //android.view.ViewGroup[@content-desc="tab_NOTIFICATION, "]
  Log    ${tombol}
  : FOR    ${loopCount}    IN RANGE    0    20
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="Notification not found"]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //android.view.ViewGroup[@content-desc="tab_NOTIFICATION, "]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # masuk halaman notifikasi
  Wait Until Page Contains Element    //android.widget.TextView[@text="Notification not found"]   ${timeout}

Explore More
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_more, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_more, "]
  # pilih T&C
  Wait Until Page Contains Element    //android.widget.TextView[@text="Terms & Conditions"]   ${timeout}
  Click Element    //android.widget.TextView[@text="Terms & Conditions"]
  # masuk halaman T&C
  Wait Until Page Contains Element    //android.widget.TextView[@text="Terms & Conditions"]   ${timeout}
  Sleep    4s
  Press Keycode    4    #back
  # klik more
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_more, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_more, "]
  # pilih privacy policy
  Wait Until Page Contains Element    //android.widget.TextView[@text="Privacy Policy"]   ${timeout}
  Click Element    //android.widget.TextView[@text="Privacy Policy"]
  # masuk halaman privacy policy
  Wait Until Page Contains Element    //android.widget.TextView[@text="Privacy Policy"]     ${timeout}
  Sleep    4s
  Press Keycode    4    #back
  # klik more
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_more, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_more, "]
  # pilih contact us
  Wait Until Page Contains Element    //android.widget.TextView[@text="Contact Us"]   ${timeout}
  Click Element    //android.widget.TextView[@text="Contact Us"]
  # masuk contact us
  Wait Until Page Contains Element    //android.widget.TextView[@text="Contact Us"]   ${timeout}
  Sleep    4s
  Press Keycode    4    #back
  # klik more
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_more, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_more, "]
  # logout
  Wait Until Page Contains Element    //android.widget.TextView[@text="Logout"]   ${timeout}
  Click Element    //android.widget.TextView[@text="Logout"]
  # berhasil logout
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
