*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    DateTime

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
${cari_webinar}   Workshop Update ANC & USG


*** Keywords ***
Explore Webinar
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'buttonWebinar')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'buttonWebinar')]
  #masuk ke halaman webinar
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Webinar')]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_webinar_search, "]   ${timeout}
  # scroll kebawah 3x
  ${lebarx}    Get Window Width
  ${tinggiy}   Get Window Height
  ${lebarx}   Convert To Integer    ${lebarx}
  ${tinggiy}  Convert To Integer    ${tinggiy}
  ${lebars}   Evaluate    ${lebarx}/2
  ${tinggis}    Evaluate    ${tinggiy} - 200
  ${x1-regis}   Convert To String    ${lebars}
  ${x2-regis}   Convert To String    ${lebars}
  ${y1-regis}   Convert To String    ${tinggis}
  ${y2-regis}   Evaluate    ${tinggis} - 500
  #swipe sampai dapat tombol Register
   : FOR    ${loopCount}    IN RANGE    0    2
   \    Swipe    ${x1-regis}    ${y1-regis}    ${x2-regis}    ${y2-regis}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s

Cari Webinar
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_webinar_search, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_webinar_search, "]
  # masuk ke halaman pencarian
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search, "]   ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_search, "]    ${cari_webinar}
  Hide Keyboard
  Sleep    2s

Detil Webinar
  #masuk ke webinar yang dicari
  Wait Until Page Contains Element    //android.widget.TextView[@text="${cari_webinar}"]    ${timeout}
  Click Element    //android.widget.TextView[@text="${cari_webinar}"]
  # masuk detail webinar
  Wait Until Page Contains Element    //android.widget.TextView[@text="Webinar Recorded"]   ${timeout}
  # like webinar
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_like, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_like, "]
  Click Element    //android.view.ViewGroup[@content-desc="button_like, "]
  # share
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_share, "]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_share, "]
  Wait Until Page Contains Element    //android.widget.TextView[@resource-id="android:id/title_default"]    ${timeout}
  Press Keycode    4    #back
  # # submit question - open chat
  # Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_go_to_chat, "]   ${timeout}
  # Click Element    //android.view.ViewGroup[@content-desc="button_go_to_chat, "]
  # # Wait Until Page Contains Element    //android.widget.Button[@text="Opsi lainnya"]   ${timeout}
  # # back ke webinar video
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]     ${timeout}
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_search, "]
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  \    ${loopCount}    Set Variable    ${loopCount}+1
  \    Sleep    2s
  # kembali ke list webinar
  Wait Until Page Contains Element    //android.widget.TextView[@content-desc="button_search, "]    ${timeout}
  Click Element    //android.widget.TextView[@content-desc="button_search, "]
  Press Keycode    4    #back
