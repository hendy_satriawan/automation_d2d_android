*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary
Library    DateTime
Library    FakerLibrary

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variables ***
${course_title}   Test CME Manual
${source_cme}   Internal
${skp_poin}   3
${judul_cme}    Quiz Everlasting


*** Keywords ***
Masuk CME
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonCme, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="buttonCme, "]
  #mulai CME
  Wait Until Page Contains Element    //android.widget.TextView[@text="CME"]    ${timeout}

Isi Kuis
  # ambil judul CME
  # kondisi ditambahkan element & ambil judul otomatis
  ${ada_judul}    Run Keyword And Return Status    //android.widget.TextView[@content-desc="titleCme"]
  Run Keyword If    '${ada_judul}' == 'True'    Run Keywords    Wait Until Page Contains Element    //android.widget.TextView[@content-desc="titleCme"]
  ...   AND   ${judul_cme}    Get Text    //android.widget.TextView[@content-desc="titleCme"]
  ...   AND   Set Global Variable    ${judul_cme}
  ...   AND   Log    ${judul_cme}
  ...   AND   Wait Until Page Contains Element    //android.widget.TextView[@text="${judul_cme}"]    5s
  ...   AND   Click Element    //android.widget.TextView[@text="${judul_cme}"]
  ...   AND   Wait Until Page Contains Element    //android.widget.TextView[@text="${judul_cme}"]
  # kondisi dengan menggunakan
  Run Keyword Unless    ${ada_judul}    Run Keywords    Log    ${judul_cme}
  ...   AND   Wait Until Page Contains Element    //android.widget.TextView[@text="${judul_cme}"]    5s
  ...   AND   Click Element    //android.widget.TextView[@text="${judul_cme}"]
  ...   AND   Wait Until Page Contains Element    //android.widget.TextView[@text="${judul_cme}"]
  # cek halaman cme
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_start_quiz, "]
  # masuk ke kuis
  Click Element    //android.view.ViewGroup[@content-desc="button_start_quiz, "]
  Wait Until Page Contains Element    //android.widget.TextView[@text="Questions"]
  #pilih true
  ${cekisikuis}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="True"]
  Run Keyword If    ${cekisikuis}    Click Element    //android.widget.TextView[@text="True"]
  # submit
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_submit, "]
  Click Element    //android.view.ViewGroup[@content-desc="button_submit, "]
  # failed
  Wait Until Page Contains Element    //android.widget.TextView[@text="Failed"]
  Click Element    //android.view.ViewGroup[@content-desc="button_ok, "]

Add SKP Manual
  # masuk ke halaman skp manual
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonCmeKursus, "]
  Click Element    //android.view.ViewGroup[@content-desc="buttonCmeKursus, "]
  Wait Until Page Contains Element    //android.widget.TextView[@text="CME Course"]
  # isi title
  ${no}=    Numerify   text=##
  ${course_title}=    Set Variable    ${course_title} ${no}
  Input Text    //android.widget.EditText[@content-desc="input_title, "]    ${course_title}
  Hide Keyboard
  # input source
  # =Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_course"]
  Input Text    //android.widget.EditText[@content-desc="input_course, "]    ${source_cme}
  Hide Keyboard
  # input skp poin
  # =Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_skp_point"]
  Input Text    //android.widget.EditText[@content-desc="input_skp_point, "]    ${skp_poin}
  Hide Keyboard
  # input certificate date
  # =Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_date"]
  Click Element    //android.widget.EditText[@content-desc="input_date, "]
  Wait Until Page Contains Element    //android.widget.Button[@resource-id="android:id/button1"]
  Click Element    //android.widget.Button[@resource-id="android:id/button1"]
  # upload cerificate
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_show_choices, "]
  Click Element    //android.view.ViewGroup[@content-desc="button_show_choices, "]
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_camera, "]   ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="button_camera, "]
  Permission_Camera     #allow permission camera
  Permission_Gallery    #allow permission Galery
  # Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_camera"]
  # Click Element    //android.view.ViewGroup[@content-desc="button_camera"]
  Wait Until Page Does Not Contain Element    //android.widget.Button[contains(@resource-id,'android:id/button2')][@text='CAMERA']    10s
  Sleep    2s
  : FOR    ${loopCount}    IN RANGE    0    5
  \    ${foto}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.sec.android.app.camera:id/okay')]
  \    Run Keyword If    ${foto}    Exit For Loop
  \    Press Keycode    80    #focus camera
  \    Press Keycode    27    #capture
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.sec.android.app.camera:id/okay')]
  Click Element    //android.widget.TextView[contains(@resource-id,'com.sec.android.app.camera:id/okay')]
  # upload
  Wait Until Page Contains Element    //android.widget.TextView[@text="Upload"]
  Click Element    //android.widget.TextView[@text="Upload"]
  # cek berhasil upload
  Wait Until Page Contains Element    //android.widget.ImageView[@index="0"]
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_download, "]
  Click Element    //android.view.ViewGroup[@content-desc="button_download, "]
  Sleep    2s
  Wait Until Page Contains Element    //android.widget.TextView[@text="Sent Email"]
  Click Element    //android.widget.TextView[@text="Sent Email"]

Kembali ke homepage CME dari skp manual
  # back ke homepage cme
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_back, "]
  : FOR    ${loopCount}    IN RANGE    0    2
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="CME"]    ${timeout}
  \    Run Keyword If    ${el}    Exit For Loop
  \    Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  \    Sleep    2s
  \    ${loopCount}    Set Variable    ${loopCount}+1
  Sleep    1s
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_cme_history, "]

History CME
  Click Element    //android.view.ViewGroup[@content-desc="button_cme_history, "]
  # masuk halaman history
  Wait Until Page Contains Element    //android.widget.TextView[@text="History"]
  # masuk ke tab skp manual
  Wait Until Page Contains Element    //android.widget.TextView[@text="MANUAL SKP"]
  Click Element    //android.widget.TextView[@text="MANUAL SKP"]

Kembali ke Homepage CME dari History
  # back ke homepage cme
  Click Element    //android.view.ViewGroup[@content-desc="button_back, "]
  Wait Until Page Contains Element    //android.widget.TextView[@text="CME"]    ${timeout}
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_cme_history, "]
