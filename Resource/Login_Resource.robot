*** Setting ***
Library    AppiumLibrary
Library    BuiltIn
Library    DatabaseLibrary

Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Permission_Resource.robot

*** Variable ***

${EMAIL_LOGIN_VALID}   qatest1@yopmail.com
${PASSWORD_LOGIN_VALID}   123456
${nama_profile}   Fetuna CH         #prod
# ${nama_profile}   HAMDANI CHRISYANTO      #stag
# data login fb
${email_login_FB}   fetzune@gmail.com
${pass_login_FB}    Awan@123

${email_login_google}   hendy.satriawan@gmail.com

${EMAIL_LOGIN_EVENT}    ptgue.devbravo@gmail.com
${PASSWORD_LOGIN_EVENT}    guecuti
*** Keywords ***
Login D2D
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # lakukan login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
  #input data login
  Input Text    //android.widget.EditText[contains(@text,'e-mail')]    ${EMAIL_LOGIN_VALID}
  Hide Keyboard
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
  Sleep    1s
  Input Text    //android.widget.EditText[contains(@text,'password')]    ${PASSWORD_LOGIN_VALID}
  Hide Keyboard
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
  Sleep    2s
  # Click Element    //android.widget.TextView[contains(@text,'LOGIN')]
  #klik tombol login -> harus 2x
  : FOR    ${loopCount}    IN RANGE    0    2
  \    Sleep    2s
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]
  \    Run Keyword If    ${el}    Click Element    //android.widget.TextView[contains(@text,'LOGIN')]
  \    Run Keyword If    '${el}' == 'False'    Exit For Loop
  \    ${loopCount}    Set Variable    ${loopCount}+1
  #cek halaman feed - berhasil login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]    ${timeout}
  # cek list feed
  ${listdata}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]   10s
  Run Keyword If    ${listdata}    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]    ${timeout}
  Run Keyword If    '${listdata}' == 'False'   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Live')]    ${timeout}



Login D2D Dengan Data Facebook
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # lakukan login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
  #masuk ke facebook
  Wait Until Page Contains Element    //android.view.ViewGroup[contains(@content-desc,'btnloginFB, ')]   ${timeout}
  Click Element    //android.view.ViewGroup[contains(@content-desc,'btnloginFB, ')]
  ${cekakun}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.Button[@text="Masuk"]    10s
  Run Keyword If    ${cekakun}    Run Keywords    Wait Until Page Contains Element    //android.widget.EditText[@resource-id="m_login_email"]    ${timeout}
  ...   AND     Click Element    //android.widget.EditText[@resource-id="m_login_email"]
  ...   AND     Input Text    //android.widget.EditText[@resource-id="m_login_email"]    ${email_login_FB}
  ...   AND     Hide Keyboard
  ...   AND     Sleep    1s
  ...   AND     Wait Until Page Contains Element    //android.widget.EditText[@resource-id="m_login_password"]    ${timeout}
  ...   AND     Click Element    //android.widget.EditText[@resource-id="m_login_password"]
  ...   AND     Input Text    //android.widget.EditText[@resource-id="m_login_password"]    ${pass_login_FB}
  ...   AND     Hide Keyboard
  ...   AND     Sleep    1s
  ...   AND     Wait Until Page Contains Element    //android.widget.Button[@text="Masuk"]     ${timeout}
  ...   AND     Click Element    //android.widget.Button[@text="Masuk"]
  #konfirmasi login dengan FB - pilih lanjutkan
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'m-future-page-header-title')]    ${timeout}
  Wait Until Page Contains Element    //android.widget.Button[@resource-id="u_0_1"]   ${timeout}
  Click Element    //android.widget.Button[@resource-id="u_0_1"]
  #cek halaman feed - berhasil login
  # cek list feed
  ${listdata}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]   10s
  Run Keyword If    ${listdata}    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]    ${timeout}
  Run Keyword If    '${listdata}' == 'False'   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Live')]    ${timeout}

Login D2D Dengan Data Google
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # lakukan login
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
  #masuk ke google
  Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="btnloginGoogle, "]    ${timeout}
  Click Element    //android.view.ViewGroup[@content-desc="btnloginGoogle, "]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.google.android.gms:id/main_title')][@text='Pilih akun']    ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.google.android.gms:id/subtitle')][@text='untuk melanjutkan ke D2D']    ${timeout}
  #pilih akun google
  Wait Until Page Contains Element    //android.widget.TextView[contains(@resource-id,'com.google.android.gms:id/account_name')][@text='${email_login_google}']    ${timeout}
  Click Element    //android.widget.TextView[contains(@resource-id,'com.google.android.gms:id/account_name')][@text='${email_login_google}']
  #cek halaman feed - berhasil login
  # cek list feed
  ${listdata}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]   10s
  Run Keyword If    ${listdata}    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]    ${timeout}
  Run Keyword If    '${listdata}' == 'False'   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Live')]    ${timeout}

Pilih Subscription
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'SUBSCRIPTION')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Select Topics')]    ${timeout}
  #pilih spesialis anak
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Spesialis Anak')]   ${timeout}
  Click Element    //android.widget.TextView[contains(@text,'Spesialis Anak')]
  Sleep    1s
  Click Element    //android.widget.TextView[contains(@text,'SUBMIT')]

Forgot Password
  # cek sudah login atau belum
  ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]
  Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
  # lakuka forgot password
  Wait Until Page Contains Element    //android.widget.TextView[@content-desc="forgot_password, "]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
  #masuk ke halaman lupa password
  Click Element    //android.widget.TextView[@content-desc="forgot_password, "]
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]   ${timeout}
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Forgot Password')]    ${timeout}
  Input Text    //android.widget.EditText[@content-desc="input_email, "]    ${email_regis}
  Hide Keyboard
  Sleep    1s
  : FOR    ${loopCount}    IN RANGE    0    2
  \    Sleep    2s
  \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_submit, "]
  \    Run Keyword If    ${el}    Click Element    //android.view.ViewGroup[@content-desc="button_submit, "]
  \    Run Keyword Unless    ${el}    Exit For Loop
  \    Sleep    1s
  \    ${loopCount}    Set Variable    ${loopCount}+1
  # Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_submit, "]    ${timeout}
  # Click Element    //android.view.ViewGroup[@content-desc="button_submit, "]
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Check Your Email!')]    ${timeout}
  #ambil kode dulu di email
  Press Keycode    3    #keluar ke home
  #buka chrome
  Wait Until Page Contains Element    //android.widget.FrameLayout[contains(@content-desc,'Chrome')]    ${timeout}
  Click Element    //android.widget.FrameLayout[contains(@content-desc,'Chrome')]
  #newtab
  Wait Until Page Contains Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/tab_switcher_button"]    ${timeout}
  Click Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/tab_switcher_button"]
  Wait Until Page Contains Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/new_tab_button"]    ${timeout}
  Click Element    //android.widget.ImageButton[@resource-id="com.android.chrome:id/new_tab_button"]
  #cek halaman awal google
  #masuk yopmail
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]   5s
  Click Element    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/search_box_text')]
  Input Text    //android.widget.EditText[contains(@resource-id,'com.android.chrome:id/url_bar')]    yopmail.com
  Press Keycode    66
  Wait Until Page Contains Element    //android.widget.EditText[contains(@resource-id,'login')]   ${timeout}
  Clear Text    //android.widget.EditText[contains(@resource-id,'login')]
  Input Text    //android.widget.EditText[contains(@resource-id,'login')]    ${email_regis}
  Press Keycode    66
  Hide Keyboard
  Sleep    1s
  Wait Until Page Contains Element    //android.widget.Button[contains(@index,'0')][@displayed="true"]
  Click Element    //android.widget.Button[contains(@index,'0')][@displayed="true"]
  Wait Until Page Contains Element    //android.view.View[contains(@text,'today')]    ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'m1')]    ${timeout}
  #buka email
  Click Element    //android.view.View[contains(@resource-id,'m1')]
  Wait Until Page Contains Element    //android.view.View[contains(@text,'D2D Password Change Confirmation')]   ${timeout}
  Wait Until Page Contains Element    //android.view.View[contains(@index,'1')]   ${timeout}
  Sleep    2s
  ${code}   Get Text    xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View[3]/android.view.View/android.view.View[2]
  Log    ${code}
  #back & hapus semua email
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'rethome')]   ${timeout}
  Click Element    //android.view.View[contains(@resource-id,'rethome')]
  Sleep    2s
  Wait Until Page Contains Element    //android.view.View[contains(@resource-id,'e0')]    ${timeout}
  Click Element    //android.view.View[contains(@resource-id,'e0')]
  Sleep    2s
  Click Element    //android.view.View[contains(@text,'javascript:suppr_sel();')]
  #kembali ke aplikasi D2D - dengan recent apps
  Sleep    1s
  Press Keycode    187  #recent apps
  Sleep    1s
  Press Keycode    187  #recent apps
  Sleep    1s
  Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Check Your Email!')]    ${timeout}
  Hide Keyboard
  Sleep    1s
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_code, "]   ${timeout}
  Click Element    //android.widget.EditText[@content-desc="input_code, "]
  Input Text    //android.widget.EditText[@content-desc="input_code, "]    ${code}
  Hide Keyboard
  Sleep    1s
  Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_password, "]   ${timeout}
  Click Element    //android.widget.EditText[@content-desc="input_password, "]
  Input Text    //android.widget.EditText[@content-desc="input_password, "]    ${pass_regis_update}
  Hide Keyboard
  Sleep    1s
   Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_confirm_password, "]   ${timeout}
   Click Element    //android.widget.EditText[@content-desc="input_confirm_password, "]
   Input Text    //android.widget.EditText[@content-desc="input_confirm_password, "]    ${pass_regis_update}
   Hide Keyboard
   Sleep    1s
   #klik tombol submit
   #klik tombol submit -> harus 2x
   : FOR    ${loopCount}    IN RANGE    0    20
   \    Sleep    1s
   \    ${el}    Run Keyword And Return Status    Page Should Not Contain Element    //android.view.ViewGroup[@content-desc="button_submit, "]
   \    Run Keyword If    ${el}    Exit For Loop
   \    Click Element    //android.view.ViewGroup[@content-desc="button_submit, "]
   \    Sleep    1s
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}

Login D2D After Forgot
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
   #input data login
   Input Text    //android.widget.EditText[contains(@text,'e-mail')]    ${email_regis}
   Hide Keyboard
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
   Sleep    1s
   Input Text    //android.widget.EditText[contains(@text,'password')]    ${pass_regis_update}
   Hide Keyboard
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
   Sleep    2s
   # Click Element    //android.widget.TextView[contains(@text,'LOGIN')]
   #klik tombol login -> harus 2x
   : FOR    ${loopCount}    IN RANGE    0    20
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]
   \    Run Keyword If    ${el}    Click Element    //android.widget.TextView[contains(@text,'LOGIN')]
   \    Run Keyword Unless    ${el}    Exit For Loop
   \    Sleep    1s
   \    ${loopCount}    Set Variable    ${loopCount}+1
   #cek halaman feed - berhasil login
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Feeds')]    ${timeout}

 Logout D2D using hidden button
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="buttonProfile, "]     ${timeout}
   Click Element    //android.view.ViewGroup[@content-desc="buttonProfile, "]
   # masuk halaman profile
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_bookmark, "]    ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_download, "]   ${timeout}
   # klik logout
   ${lebarx}    Get Window Width
   ${tinggiy}   Get Window Height
   ${lebarx}   Convert To Integer    ${lebarx}
   ${tinggiy}  Convert To Integer    ${tinggiy}
   ${lebars}   Evaluate    ${lebarx}/2
   ${tinggis}    Evaluate    ${tinggiy} - 200
   ${x1-artikel}   Convert To String    ${lebars}
   ${x2-artikel}   Convert To String    ${lebars}
   ${y1-artikel}   Convert To String    ${tinggis}
   ${y2-artikel}   Evaluate    ${tinggis} - 500
   #Scroll halaman sampai dapat tombol hiden logout
   : FOR    ${loopCount}    IN RANGE    0    20
   \    ${el}    Run Keyword And Return Status    Wait Until Element Is Visible    //android.widget.TextView[contains(@text,'Specialization')]
   \    Sleep    1s
   \    Run Keyword If    ${el}    Exit For Loop
   \    Swipe    ${x1-artikel}    ${y1-artikel}    ${x2-artikel}    ${y2-artikel}
   \    ${loopCount}    Set Variable    ${loopCount}+1
   Sleep    1s
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="button_logout_testing, "]
   Click Element    //android.view.ViewGroup[@content-desc="button_logout_testing, "]
   # cek halaman login
   Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]    ${timeout}
   Wait Until Page Contains Element    //android.view.ViewGroup[@content-desc="btnlogin, "]    ${timeout}

Login Akun Terdaftar Event
   # cek sudah login atau belum
   ${cek_login}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_email, "]
   Run Keyword Unless    ${cek_login}    Logout D2D using hidden button
   # lakukan login
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Welcome!')]   ${timeout}
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
   #input data login
   Input Text    //android.widget.EditText[contains(@text,'e-mail')]    ${EMAIL_LOGIN_EVENT}
   Hide Keyboard
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
   Sleep    1s
   Input Text    //android.widget.EditText[contains(@text,'password')]    ${PASSWORD_LOGIN_EVENT}
   Hide Keyboard
   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]    ${timeout}
   Sleep    2s
   # Click Element    //android.widget.TextView[contains(@text,'LOGIN')]
   #klik tombol login -> harus 2x
   : FOR    ${loopCount}    IN RANGE    0    2
   \    Sleep    2s
   \    ${el}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'LOGIN')]
   \    Run Keyword If    ${el}    Click Element    //android.widget.TextView[contains(@text,'LOGIN')]
   \    Run Keyword If    '${el}' == 'False'    Exit For Loop
   \    ${loopCount}    Set Variable    ${loopCount}+1
   #cek halaman feed - berhasil login
   # cek list feed
   ${listdata}   Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]   10s
   Run Keyword If    ${listdata}    Wait Until Page Contains Element    //android.widget.TextView[contains(@content-desc,'iconBookmark, ')]    ${timeout}
   Run Keyword If    '${listdata}' == 'False'   Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'Live')]    ${timeout}
