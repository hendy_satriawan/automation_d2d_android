*** Setting ***

Library    AppiumLibrary
Library    BuiltIn
Resource    ../Resource/Capability_Device_Resource.robot
Resource    ../Resource/Login_Resource.robot
Resource    ../Resource/Register_Resource.robot
Resource    ../Resource/Permission_Resource.robot
Resource    ../Resource/Feeds_Resource.robot
Resource    ../Resource/Explore_Resource.robot
Resource    ../Resource/CME_Resource.robot
Resource    ../Resource/Webinar_Resource.robot
Resource    ../Resource/Profile_Resource.robot

*** Test Cases ***
1.Register
  [Documentation]   Register Dengan Data yang sudah di whitelist
  Buka Apps D2D Real Device
  Register D2D
  Aktivasi Akun
  Close Application
  Clear Appium cache FIle

2.Login
  [Documentation]   Login dengan Data yang diregister
  Buka Apps D2D Real Device
  Login D2D
  Close Application
  Clear Appium cache FIle

3.Login Facebook
  [Documentation]   Login dengan akun Facebook
  Buka Apps D2D Real Device
  Login D2D Dengan Data Facebook
  Close Application
  Clear Appium cache FIle

4.Login Google
  [Documentation]   Login dengan akun Google
  Buka Apps D2D Real Device
  Login D2D Dengan Data Google
  Close Application
  Clear Appium cache FIle

3.Lupa Password - masih error pas ambil code di chrome yopmail
  [Documentation]   Lupa password dengan akun yang sudah diregister
  Buka Apps D2D Real Device
  Forgot Password
  Login D2D After Forgot
  Close Application
  Clear Appium cache FIle

5.Feeds - Journal
  [Documentation]   Buka Feeds & Detail Feeds Journal
  Buka Apps D2D Real Device
  Login D2D
  Feeds View Jurnal
  Feeds View Jurnal Detail
  Kembali Dari PDF ke Detail Feed
  Bookmark Feed Detail Jurnal
  Unbookmark Feed Detail Jurnal
  Kembali Dari Detail Jurnal ke List Feed

6.Feeds - Guideline
  [Documentation]   Buka Feeds & Detail Feeds Guideline
  Feeds View Guideline
  ${guide}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[@text="guideline"]
  Run Keyword If    '${guide}' == 'True'    Run Keywords    Feeds View Guideline Detail
  ...   AND   Kembali Dari Detail Guideline ke List Feed
  ...   AND   Bookmark Feed List Guideline
  ...   AND   Unbookmark Feed List Guideline
  Run Keyword If    '${guide}' == 'False'    Log    Tidak ada Guideline di Feeds

7.Feeds - Video
  [Documentation]   Buka Feeds & Detail Feeds Video
  Feeds View Video
  ${video}    Run Keyword And Return Status    Wait Until Page Contains Element    //android.widget.TextView[contains(@text,'${judul_feed_Video}')]
    Run Keyword If    '${video}' == 'True'    Run Keywords    Feeds View Video Detail
    ...   AND   Bookmark Feed List Video
    ...   AND   Unbookmark Feed List Video
    Run Keyword If    '${video}' == 'False'    Log    Tidak ada Video di Feeds

8.CME
  [Documentation]   Buka CME & Add SKP
  Masuk CME
  Isi Kuis
  Add SKP Manual
  Kembali ke homepage CME dari skp manual
  History CME
  Kembali ke Homepage CME dari History

8.Explore - Event
  [Documentation]   Buka Explore & Pilih Event
  Explore Event List
  Event detail
  Bookmark Event Detail
  Share Event
  Add Event To Calendar
  Unbookmark Event Detail
  Kembali ke List Event
  Search Event
  Kembali dari pencarian event ke list event
  Filter Event

9.Explore - Learning
  [Documentation]   Buka Explore & Pilih Learning
  Explore Learning List
  Explore Detail List
  Explore Tab Bookmark
  Search Konten Learning
  Cari Spesialis

10.Explore - Request Journal
  [Documentation]   Buka Explore & Pilih Request Journal
  Explore Request Journal

11.Explore - Obat A to Z
  [Documentation]   Buka Explore & Pilih Obat A to Z
  Explore Obat A to Z
  # Buka Detil Obat A to Z
  Cari Obat A to Z

12.Webinar
  [Documentation]   Buka Webinar
  Explore Webinar
  Cari Webinar
  Detil Webinar

13.Profile
  [Documentation]   masuk ke halaman profile lalu explore
   Explore Profile
   Tab Bookmark
   Tab Download
   Save Profile
   Tab Notifikasi
   Explore More
   Close Application
   Clear Appium cache FIle

##Spesial Case

12.Event - Materi Download & Ecertificate
  [Documentation]    masuk dengan akun lain, lalu downlod materi event terdaftar
  Buka apps D2D real device
  Login Akun Terdaftar Event
  Explore Event Dengan Materi
  Kembali ke List Event Joined
  Explore Ecertificate
  Kembali ke List Event Dari Ecertificate
  Close Application
  Clear Appium cache FIle
