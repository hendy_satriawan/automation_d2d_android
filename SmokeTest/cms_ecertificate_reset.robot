*** Setting ***
Library    SeleniumLibrary


*** Variables ***
${link}    https://dashboard.d2d.co.id/
${browser}    Chrome
${implicit_wait}    Set Browser Implicit Wait    10s
${timeout}    30s
${username}   superadmin
${password}   pass@word

*** Keywords ***
Buka Browser CMS D2D
  ${options}=    Evaluate  sys.modules['selenium.webdriver.chrome.options'].Options()    sys
  Call Method     ${options}    add_argument    --disable-notifications
  ${driver}=    Create Webdriver    Chrome    options=${options}
  Go To    ${link}
  # Maximize Browser Window
  Wait Until Page Contains Element    //div[@class='form-group login-submit']/button[@type='submit']
  Wait Until Page Contains Element    //div[@class='login-box']//form[@action='https://dashboard.d2d.co.id/auth/authenticate']//input[@name='username']
  Sleep    5s

Login CMS
  # input username
  Wait Until Page Contains Element    //div[@class='login-box']//form[@action='https://dashboard.d2d.co.id/auth/authenticate']//input[@name='username']   ${timeout}
  # Click Element    //div[@class='login-box']//form[@action='https://dashboard.d2d.co.id/auth/authenticate']//input[@name='username']
  Input Text    //div[@class='login-box']//form[@action='https://dashboard.d2d.co.id/auth/authenticate']//input[@name='username']    ${username}
  Sleep    2s
  # input password
  Wait Until Page Contains Element    //div[@class='login-box']//form[@action='https://dashboard.d2d.co.id/auth/authenticate']//input[@name='password']   ${timeout}
  # Click Element    //div[@class='login-box']//form[@action='https://dashboard.d2d.co.id/auth/authenticate']//input[@name='password']
  Input Text    //div[@class='login-box']//form[@action='https://dashboard.d2d.co.id/auth/authenticate']//input[@name='password']    ${password}
  Sleep    2s
  # klik login
  Wait Until Page Contains Element    //div[@class='form-group login-submit']/button[@type='submit']    ${timeout}
  Click Element    //div[@class='form-group login-submit']/button[@type='submit']
  Sleep    4s


*** Test Cases ***
1. Buka CMS Prod
  Buka Browser CMS D2D
  Login CMS
